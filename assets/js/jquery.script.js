(function ($) {

	'use strict';

	// google map load after all page finish
	var myCenter=new google.maps.LatLng(-6.91049437732718,107.625922129783);
	var marker=new google.maps.Marker({
	    position:myCenter
	});

	function initialize() {
	  var mapProp = {
	      center:myCenter,
	      zoom: 17,
	      'disableDefaultUI': false,
	      draggable: false,
	      scrollwheel: false,
	      mapTypeId:google.maps.MapTypeId.ROADMAP
	  };

	  var map=new google.maps.Map(document.getElementById("map"),mapProp);
	  marker.setMap(map);

	  google.maps.event.addListener(marker, 'click', function() {
	      
	    infowindow.setContent(contentString);
	    infowindow.open(map, marker);
	    
	  }); 

	}

	// Scrolling to team-tab
	$(function() {
		$('.btn-tab').bind('click', function (e) {
			var $anchor = $('#map-tab');

			$('html, body').stop().animate({ 
				scrollTop: $($anchor).offset().top - 93
			},
				500,
				'linear'
			);
			e.preventDefault();
		});
	});

	$("#map-id").on('shown.bs.tab', function(e) {

		// google.maps.event.trigger( map , 'resize');
		initialize();

	});

	// Closing map & hide map
	$('.close-map').on( 'click', function() {
		$('.tab-content').slideUp('fast', function() {
			$('.map-wrapper').removeClass('active');
			$('.tab-content').show();
		});
	});

	// Change background
	var totalCount = 3;
	function changeIt(){
		var num = Math.ceil( Math.random() * totalCount );
		$('.homepage-img').css('background-image', 'url("images/newslide/'+num+'.jpg ")' );

	}

	changeIt();


	var owl = $('#owl-information');

	// Information-slider
	owl.owlCarousel({
		items: 3,
		itemsDesktop: [1000, 3],
		itemsDesktopSmall: [900,3 ],
		itemsTablet: [600, 3],
		itemsMobile: [450, 1],
		pagination: false,
		slideSpeed : 4100,
        paginationSpeed : 2800,
        rewindSpeed : 5800,
	});

	// Custom navigations
	$(".more-article").click(function(){
		owl.trigger('owl.next');
	});

	$(".more-article-prev").click(function(){
		owl.trigger('owl.prev');
	});

	$(".more-article").on('mouseover', function(e) {
		owl.trigger('owl.play', 1800);
	});
	$(".more-article").on('mouseleave', function(e) {
		owl.trigger('owl.stop');
	});

	$(".more-article-prev").on('mouseover', function(e) {
		// owl.trigger('owl.rtl', true);
		owl.trigger('owl.play', 1800);
	});


	//Slider
	// $('.homepage-slider').owlCarousel({
		// autoPlay: 5000,
		// autoPlay: false,
		// singleItem: true,
		// autoHeight: true,
		// pagination: false,
		// transitionStyle: 'fade'
	// });


	// toggle more article
	// $(".more-article").click(function(e) {
		// e.preventDefault();
	
		// $('#list-box-information').toggle('ease');	
	// });

	// rwd navigation
	$('.main-navigation ul').clone(false).find("ul,li").removeAttr("id").remove(".submenu").appendTo($(".mobile-nav"));
	$('.nav-btn').on("click", function() {
		$('.mobile-menu').collapse({
			toggle: false
		});
	});



	// var countChild = $('#list-box-information').children().length;
	// var containerInfo = $('.list-information').width();
	// var valueSum = containerInfo / countChild;
	// $(".auto-width").width( (containerInfo / countChild) - 9 );
	// run functions on window resize


	

	new WOW().init();


	
})(jQuery);